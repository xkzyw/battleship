class Ship():
	def __init__(self, coordinate, color_in):
		self.coordinate = coordinate
		self.color = color_in
		self.show = False

	@property
	def show(self):
		return self.__show

	@show.setter
	def show(self, val: bool):
		if not isinstance(val, bool):
			raise TypeError("Non instanza di bool ricevuto")

		self.__show = val

	@property
	def coordinate(self):
		return self.__cord

	@coordinate.setter
	def coordinate(self, coordinate_in):
		if not isinstance(coordinate_in, str):
			raise TypeError("Non instanza di stringa ricevuto")

		if len(coordinate_in) != 2:
			raise ValueError("Cordinata deve essere lungo 2 caratteri")

		if not coordinate_in[0].isalpha():
			raise ValueError("Il primo carattere della cordinata deve essere una lettera")

		if not coordinate_in[1].isnumeric():
			raise ValueError("Il secondo carattere della cordinata deve essere un numero")

		self.__cord = coordinate_in

	@property
	def color(self):
		return self.__color

	@color.setter
	def color(self, color_in):
		if isinstance(color_in, str):
			self.__color = color_in
		else:
			raise TypeError("il colore va messo come striga")

	def __str__(self):
		return f"{self.coordinate}"

	def __repr__(self):
		return self.__str__()

	def __eq__(self, other):
		if not isinstance(other, Ship):
			return False

		if other.coordinate == self.coordinate:
			return True


if __name__ == "__main__":
	l = [Ship(("A", 2), "black"), Ship(("B", 2), "black")]
	print(Ship(("A", 2), "black") in l)
