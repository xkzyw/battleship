import src.objects.player as player


class Game():
	def __init__(self, player1: player.Player, player2: player.Player, duration: int = 0, max_ship: int = 0, num_ship: int = 0, config=None):
		self.player1 = player1
		self.player2 = player2
		self.duration = duration
		self.max_ship = max_ship
		self.num_ship = num_ship
		self.start = False

	@property
	def num_ship(self):
		return self.__num_ship

	@num_ship.setter
	def num_ship(self, val: int):
		if not isinstance(val, int):
			raise TypeError("Non instanza di intero trovato")

		self.__num_ship = val

	@property
	def max_ship(self):
		return self.__max_ship

	@max_ship.setter
	def max_ship(self, val: int):
		if not isinstance(val, int):
			raise TypeError("Non intero ricevuto")

		self.__max_ship = val

	@property
	def duration(self):
		return self.__duration

	@duration.setter
	def duration(self, val: int):
		if not isinstance(val, int):
			raise TypeError("Inserisci un intero")

		self.__duration = val

	@property
	def player1(self) -> player.Player:
		return self.__player1

	@player1.setter
	def player1(self, val: player.Player):
		self.__player1 = val

	@property
	def player2(self):
		return self.__player1

	@player2.setter
	def player2(self, val: player.Player):
		self.__player2 = val



