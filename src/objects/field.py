import src.objects.ship_group as ship_group
import src.objects.ship as ship
from src.objects.player import Player
import src.objects.ship_group as ship_group


class Field(object):
	def __init__(self, row: int = 10, column: int = 10):
		self.row: int = row
		self.column: int = column
		self.__shipGroups: list[ship_group.ShipGroup] = []
		self.__selected_coordinates = []
		self.__temp_coordinate = ship_group.ShipGroup([], "red")

	@property
	def temp_coordinate(self):
		return self.__temp_coordinate

	@temp_coordinate.setter
	def temp_coordinate(self, o: ship_group.ShipGroup):
		if not isinstance(o, ship_group.ShipGroup):
			raise TypeError("Non instanza di shipgroup trovato")

		for i in o.members:
			for j in o.members:
				if not ((i.coordinate[0] == j.coordinate[0]) or (i.coordinate[1] == j.coordinate[1])):
					pass

		self.__temp_coordinate = o

	@property
	def player(self):
		return self.__player

	@player.setter
	def player(self, val):
		if not isinstance(val, Player):
			raise TypeError("Non instanza di Player ricevuto")

		self.__player = val

	@property
	def selected_coordinates(self):
		return self.__selected_coordinates

	def select(self, coordinate) -> bool:
		for sg in self.shipGroups:
			if sg.find_ship(coordinate):
				s = sg.get_ship(coordinate)
				s.show = True
				self.__selected_coordinates.append(coordinate)
				return True

		return False


	@property
	def shipGroups(self):
		return self.__shipGroups

	def add_ships(self, sp: ship_group.ShipGroup):
		if not isinstance(sp, ship_group.ShipGroup):
			raise TypeError("Non instanza di ShipGoup ricevuto")

		for sg in self.shipGroups:
			if sg * sp:
				return False

		self.__shipGroups.append(sp)
		return True

	def remove_ship(self, coordinate):
		for s in self.__shipGroups:
			if s.remove_ship(coordinate):
				break

	@property
	def row(self) -> int:
		return self.__row

	@row.setter
	def row(self, val: int):
		if not isinstance(val, int):
			raise TypeError("Inserisci un numero intero")

		self.__row = val

	@property
	def column(self) -> int:
		return self.__column

	@column.setter
	def column(self, val: int):
		if not isinstance(val, int):
			raise TypeError("Inserisci un numero intero")

		self.__column = val

	@shipGroups.setter
	def shipGroups(self, value):
		self._shipGroups = value