from typing import List

from src.objects.ship import Ship


class ShipGroup:
	def __init__(self, ms: List[Ship], color):
		self.members = ms
		self.color = color

	def find_ship(self, coordinate):
		for ms in self.members:
			if ms.coordinate == coordinate:
				return True

		return False

	@property
	def color(self):
		return self.__color

	@color.setter
	def color(self, val: str):
		if not isinstance(val, str):
			raise TypeError("Non instanza di stringa ricevuto")

		self.__color = val

	@property
	def members(self):
		return self.__members

	@members.setter
	def members(self, val: List[Ship]):
		if not isinstance(val, list):
			raise TypeError("Devi passare una lista")

		for m in val:
			if not isinstance(m, Ship):
				raise TypeError("Lista contenente elementi non Ship")

		self.__members = val

	def get_ship(self, coordinate) -> Ship:
		for m in self.members:
			if m.coordinate == coordinate:
				return m

	def add_ship(self, val: Ship):
		if not isinstance(val, Ship):
			raise TypeError("Non instanza di Ship ricevuto")

		if val in self.members:
			return False

		self.__members.append(val)
		return True

	def remove_ship(self, coordinate) -> bool:
		for m in self.members:
			if m.coordinate == coordinate:
				self.members.remove(m)
				return True

		return False

	def __mul__(self, other):
		if not isinstance(other, ShipGroup):
			return False

		for m in other.members:
			if m in self.members:
				return True

		return False

	def __eq__(self, other):
		f = True
		g = True

		for m in self.members:
			if m not in other.members:
				f = False
				break

		for n in other.members:
			if n not in self.members:
				g = False
				break

		return f and g

	def __str__(self):
		return str(self.members)

	def __repr__(self):
		return self.__str__()


if __name__ == "__main__":
	a = ShipGroup([Ship("A1", "black"), Ship("B2", "black")], "black")
	b = ShipGroup([Ship("D1", "black"), Ship("B2", "black")], "black")

	print(a == b)

