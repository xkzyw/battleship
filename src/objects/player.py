from src.objects.ship import Ship
from src.objects.ship_group import ShipGroup
from typing import List

class Player():
	def __init__(self, type_in="", name="", color="", my_field=None, his_field=None):
		self.type_player = type_in
		self.name = name
		self.my_field = my_field
		self.his_field = his_field
		self.color = color

	@property
	def color(self):
		return self.__color

	@color.setter
	def color(self, val: str):
		if not isinstance(val, str):
			print("Non instanza di stringa trovato")

		self.__color = val

	@property
	def his_field(self):
		return self.__his_field

	@his_field.setter
	def his_field(self, field):
		from src.objects.field import Field
		if field is None:
			self.__his_field = Field()
			return

		if isinstance(field, Field):
			self.__his_field = field
		else:
			raise TypeError("Il parametro his_field va messo come Field")

	@property
	def type_player(self):
		return self.__type_player

	@type_player.setter
	def type_player(self, type_in):
		if isinstance(type_in, str):
			self.__type_player = type_in
		else:
			raise TypeError("Il parametro type va messo come stringa")

	@property
	def name(self):
		return self.__name

	@name.setter
	def name(self, name):
		if isinstance(name, str):
			self.__name = name
		else:
			raise TypeError("Il parametro name va messo come stringa")

	@property
	def my_field(self):
		return self.__my_field

	@my_field.setter
	def my_field(self, field):
		from src.objects.field import Field
		if field is None:
			self.__his_field = Field()
			return
		if isinstance(field, Field):
			self.__my_field = field
		else:
			raise TypeError("Il parametro my_field va messo come Field")


if __name__ == "__main__":
	from src.objects.field import Field
	player = Player("pc", "", Field(10, 10), "Yellow", [ShipGroup([Ship(("A", 1), "yellow"), Ship(("B", 2), "yellow")], "yellow"), ShipGroup([Ship(("C", 1), "yellow"), Ship(("B", 2), "yellow")], "yellow")])
