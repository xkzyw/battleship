from UI.pre_game import pre_game
from objects.game import Game
from objects.player import Player
from UI.battleship_window import BattleShip
import threading

if __name__ == "__main__":
	game = Game(player1=Player(), player2=Player())

	#pre_game(game=game).mainloop()

	#if not game.start:
	#	exit()

	BattleShip(game=game).mainloop()


