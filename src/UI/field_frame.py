import copy
import tkinter
from src.objects.field import Field
from src.objects.ship_group import Ship, ShipGroup


class Field_frame(tkinter.Frame):
	def __init__(self, master, field_in: Field, max_ship=3, width=80, height=80, mode="config", num_ship=4, **kwargs):
		super().__init__(master=master, **kwargs)

		self.__letters = [chr(x)for x in range(ord("A"), ord("Z") + 1)]

		self.draw_area: tkinter.Canvas = None
		self.xscrollbar = None
		self.top_frame = None
		self.yscrollbar = None
		self.__height = None
		self.__width = None
		self.num_ship = num_ship

		self.__selected_item = list()

		self.max_ship = max_ship

		self.width = width
		self.height = height
		self.mode = mode

		self.field = field_in
		print(self.mode)

		self.draw()

	@property
	def max_ship(self):
		return self.__max_ship

	@max_ship.setter
	def max_ship(self, val):
		if not isinstance(val, int):
			raise TypeError("Non numero ricevuto")

		if val <= 0:
			raise ValueError("Inserisci un numero positivo")

		self.__max_ship = val

	@property
	def mode(self):
		return self.__mode

	@mode.setter
	def mode(self, val):
		self.__mode = val

	@property
	def width(self):
		return self.__width

	@width.setter
	def width(self, val: int):
		if not isinstance(val, int):
			raise TypeError("Non instanza di int ricevuto")

		self.__width = val

	@property
	def height(self):
		return self.__height

	@height.setter
	def height(self, val: int):
		if not isinstance(val, int):
			raise TypeError("Non instanza di int ricevuto")

		self.__height = val


	@property
	def field(self):
		return self.__field

	@field.setter
	def field(self, val: Field):
		if not isinstance(val, Field):
			raise TypeError("Non instanza di Field ricevuto")

		self.__field = val

	def draw(self):
		self.top_frame = tkinter.Frame(self)
		self.top_frame.pack()

		self.yscrollbar = tkinter.Scrollbar(self.top_frame, orient=tkinter.VERTICAL)
		self.xscrollbar = tkinter.Scrollbar(self, orient=tkinter.HORIZONTAL)

		self.draw_area = tkinter.Canvas(self.top_frame, height=700, width=800, borderwidth=1, relief=tkinter.SUNKEN, yscrollcommand=self.yscrollbar.set, xscrollcommand=self.xscrollbar.set, scrollregion=(0, 0, self.width*self.field.column + 1, self.height*self.field.row +1))
		self.draw_area.bind("<Button-1>", self.on_click)
		self.draw_area.bind("<Button-3>", self.on_right_click)
		self.draw_area.bind("<B1-Motion>", self.on_motion)
		self.draw_area.bind("<ButtonRelease-1>", self.on_button_release)
		self.draw_area.pack(side="left")

		self.draw_field()

		self.yscrollbar.pack(side="left", expand=True, fill=tkinter.Y)
		self.yscrollbar.config(command=self.draw_area.yview)

		self.xscrollbar.pack(side="top", expand=True, fill=tkinter.X)
		self.xscrollbar.config(command=self.draw_area.xview)

	def draw_field(self):
		for i in range(self.field.row):
			for j in range(self.field.column):
				p1 = (j*self.width, i*self.height)
				p2 = (j*self.width + self.width, i*self.height)
				p3 = (j*self.width + self.width, self.height*i + self.height)
				p4 = (j*self.width, i*self.height + self.height)

				tag = f"{self.__letters[j]}{i+1}"

				self.draw_area.create_polygon(*p1, *p2, *p3, *p4, tags=tag, fill="", outline="black")

	def on_button_release(self, event):
		if self.mode == "config":
			self.add_temp_to_field()

	def add_temp_to_field(self):
		if not len(self.field.shipGroups) > self.num_ship:
			self.field.add_ships(self.field.temp_coordinate)
			self.field.temp_coordinate = ShipGroup([], "red")

	def refresh_me(self):
		if self.mode == "play-other":
			for selected in self.field.selected_coordinates:
				self.draw_area.itemconfigure(selected, fill="blue")

				for sg in self.field.shipGroups:
					if sg.find_ship(selected):
						self.draw_area.itemconfigure(selected, fill="yellow")
		elif self.mode == "config":
			for sg in self.field.shipGroups:
				for s in sg.members:
					self.draw_area.itemconfigure(s.coordinate, fill=s.color)

			for s in self.field.temp_coordinate.members:
				self.draw_area.itemconfigure(s.coordinate, fill=s.color)

	def on_right_click(self, event):
		if self.mode == "config":
			self.deselect(event)

	def on_click(self, event: tkinter.Event):
		if self.mode == "config" and not len(self.field.shipGroups) > self.num_ship:
			self.__selected_item.clear()
			self.select_one(event)
		if self.mode == "play-other":
			self.hit(event)

	def on_motion(self, event: tkinter.Event):
		if not len(self.__selected_item) >= self.max_ship and self.mode == "config" and not len(self.field.shipGroups) > self.num_ship:
			self.select_many(event)
			self.refresh_me()

	def hit(self, event, coordinate=None):
		if self.mode == "play-other":
			if coordinate is not None:
				self.field.selected_coordinates.append(coordinate)
				self.refresh_me()
				return

			a = self.draw_area.find_overlapping(event.x-2, event.y-2, event.x+2, event.y+2)
			tag = self.draw_area.gettags(a[0])

			self.field.selected_coordinates.append(tag[0])
			self.refresh_me()

	def select_one(self, event):
		if len(self.field.shipGroups) >= self.num_ship:
			return

		a = self.draw_area.find_overlapping(event.x-2, event.y-2, event.x+2, event.y+2)
		tag = self.draw_area.gettags(a[0])

		self.__selected_item.append(tag[0])

		if self.mode == "config":
			self.field.temp_coordinate = ShipGroup([Ship(tag[0], "red")], "red")
			self.refresh_me()

	def deselect(self, event):
		a = self.draw_area.find_overlapping(event.x-2, event.y-2, event.x+2, event.y+2)
		tag = self.draw_area.gettags(a[0])

		rsg = None

		for sg in self.field.shipGroups:
			if sg.find_ship(tag[0]):
				for s in sg.members:
					self.draw_area.itemconfigure(s.coordinate, fill="")
				rsg = sg

		if rsg is not None:
			self.field.shipGroups.remove(rsg)

		self.field.shipGroups = [x for x in self.field.shipGroups if len(x.members) > 0]
		print(self.field.shipGroups)
		self.refresh_me()

	def select_many(self, event):
		if len(self.field.shipGroups) >= self.num_ship:
			return
		a = self.draw_area.find_overlapping(event.x-2, event.y-2, event.x+2, event.y+2)
		tag = self.draw_area.gettags(a[0])

		if tag[0] not in self.__selected_item:
			self.__selected_item.append(tag[0])
			ships = []
			for c in self.__selected_item:
				ships.append(Ship(c, "red"))
			self.field.temp_coordinate = ShipGroup(ships, "red")

			self.refresh_me()
