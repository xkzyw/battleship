import tkinter
from src.UI.field_frame import Field_frame
from src.objects.field import Field
from src.objects.game import Game
import tkinter.messagebox as tmb


class BattleShip(tkinter.Tk):
	def __init__(self, game: Game = None):
		super().__init__()

		self.finish_button = None
		self.__game = None
		self.field2_frame: Field_frame = None
		self.field2 = None
		self.field1_frame = None
		self.field1 = None
		self.finish_click = 0

		self.game = game

		self.title("Battaglia navale")
		self.resizable(False, False)
		self.geometry(f"{self.winfo_screenwidth()-200}x{self.winfo_screenheight()-100}")
		self.main_frame = tkinter.Frame(self)
		self.main_frame.pack(fill="both", expand=True)

		self.start()

	@property
	def game(self):
		return self.__game

	@game.setter
	def game(self, val: Game):
		self.__game = val

	def start(self):
		self.field1 = Field(10, 10)
		self.field2 = Field(10, 10)

		self.game.player1.my_field = self.field1
		self.game.player1.his_field = self.field2

		self.top_frame = tkinter.Frame(self.main_frame)
		self.top_frame.pack(side="top", anchor="w")

		self.field1_frame = Field_frame(self.main_frame, self.field1, mode="config")
		self.field1_frame.pack(side="left")
		
		self.field2_frame = Field_frame(self.main_frame, self.field2, mode="config-other")
		self.field2_frame.pack(side="right")

		self.finish_button = tkinter.Button(self, text="Commincia", height=2, font="Arial 14", command=self.finish_button_clicked)
		self.finish_button.pack()

	def finish_button_clicked(self):
		if len(self.field1.shipGroups) != self.field1_frame.num_ship:
			tmb.showinfo("Navi insufficiente", f"Hai posizionato {len(self.field1.shipGroups)} su {self.field1_frame.num_ship} navi")
			return

		self.field1_frame.mode = "play"
		self.field2_frame.mode = "play-other"

		if self.finish_button == 2:
			self.finish_button.destroy()

	def end(self):
		for w in self.main_frame.winfo_children():
			w.destroy()

		self.field1 = None
		self.field2 = None


if __name__ == "__main__":
	BattleShip().mainloop()