import tkinter as tk
from tkinter import ttk
from functools import partial
#from src.objects.game import Game TODO
#from src.objects.field import Field TODO
#config dizionario in cui chiave valore passo le altre config TODO

class Impostazioni(tk.Tk):
	def __init__(self,nome="Impostazioni"):
		super().__init__()
		self.title(nome)
		self.geometry("500x600")
		self.resizable(0,0)
		self.CreateWidget()
	def changeColor(self,butto):
		if butto.cget('bg')=="Red":
			butto.config(bg="Green")
		else:
			butto.config(bg="Red")
	def CreateWidget(self):
		self.MainFrame=tk.Frame(self)
		self.MainFrame.pack()
		self.scritta1=tk.Label(self.MainFrame,text="Impostazioni",font=("Courier", 20,"bold"))
		self.scritta1.pack(ipady=15)
		self.config_label = tk.LabelFrame(self.MainFrame,text="Configurazione",font=("Courier", 10,"bold"))
		self.config_label.pack(ipady=20)
		
		self.Spazio=tk.Label(self.config_label,text="                     ")
		self.Spazio.grid(column=3,row=0,padx=10,pady=10)
		
		self.scritta2=tk.Label(self.config_label,text="Grandezza campo:",font=("Courier", 10,"bold"))
		self.scritta2.grid(column=1,row=0,padx=10,pady=10)
		self.scritta2_1=tk.Label(self.config_label,text="Colonne:")
		self.scritta2_1.grid(column=0,row=1,padx=10,pady=10)
		
		self.scritta2_1_1=tk.Label(self.config_label,text="(da 10 a 26)",font=("", 7,))
		self.scritta2_1_1.grid(column=3,row=1,padx=10,pady=10)
		
		self.colonne=tk.IntVar()
		self.colonne.set(10)
		self.spinbox_column=ttk.Spinbox(self.config_label,from_=10,to=26,wrap=True,textvariable=self.colonne,state='readonly')
		self.spinbox_column.grid(column=1,row=1,padx=10,pady=10)
		
		self.scritta2_2=tk.Label(self.config_label,text="Righe:")
		self.scritta2_2.grid(column=0,row=2,padx=10,pady=10)
		
		self.scritta2_2_1=tk.Label(self.config_label,text="(da 10 a 26)",font=("", 7,))
		self.scritta2_2_1.grid(column=3,row=2,padx=10,pady=10)
		
		self.righe=tk.IntVar()
		self.righe.set(10)
		self.spinbox_row=ttk.Spinbox(self.config_label,from_=10,to=26,wrap=True,textvariable=self.righe,state='readonly')
		self.spinbox_row.grid(column=1,row=2,padx=10,pady=10)
		
		self.scritta2_3=tk.Label(self.config_label,text="Durata partita:")
		self.scritta2_3.grid(column=0,row=3,padx=10,pady=10)
		
		self.scritta2_3_1=tk.Label(self.config_label,text="(da 5 a 10)",font=("", 7,))
		self.scritta2_3_1.grid(column=3,row=3,padx=10,pady=10)
		
		self.tempo=tk.IntVar()
		self.tempo.set(5)
		self.spinbox_row=ttk.Spinbox(self.config_label,from_=5,to=10,wrap=True,textvariable=self.tempo,state='readonly')
		self.spinbox_row.grid(column=1,row=3,padx=10,pady=10)
		
		self.scritta2_4=tk.Label(self.config_label,text="Numero navi:")
		self.scritta2_4.grid(column=0,row=4,padx=10,pady=10)
		
		self.scritta2_4_1=tk.Label(self.config_label,text="(da 5 a 10)",font=("", 7,))
		self.scritta2_4_1.grid(column=3,row=4,padx=10,pady=10)
		
		self.navi=tk.IntVar()
		self.navi.set(5)
		self.spinbox_row=ttk.Spinbox(self.config_label,from_=5,to=10,wrap=True,textvariable=self.navi,state='readonly')
		self.spinbox_row.grid(column=1,row=4,padx=10,pady=10)
		
		self.scritta2_5=tk.Label(self.config_label,text="Lunghezza navi:",font=("Courier", 10,"bold"))
		self.scritta2_5.grid(column=1,row=5,padx=10,pady=10)
		
		self.scritta2_5=tk.Label(self.config_label,text="Max:")
		self.scritta2_5.grid(column=0,row=6,padx=10,pady=10)
		
		self.scritta2_5_1=tk.Label(self.config_label,text="(da 2 a 6)",font=("", 7,))
		self.scritta2_5_1.grid(column=3,row=6,padx=10,pady=10)
		
		self.lunMax=tk.IntVar()
		self.lunMax.set(2)
		self.spinbox_row=ttk.Spinbox(self.config_label,from_=2,to=6,wrap=True,textvariable=self.lunMax,state='readonly')
		self.spinbox_row.grid(column=1,row=6,padx=10,pady=10)
		
		self.scritta2_5=tk.Label(self.config_label,text="Min:")
		self.scritta2_5.grid(column=0,row=7,padx=10,pady=10)
		
		self.scritta2_5_2=tk.Label(self.config_label,text="(da 2 a 6)",font=("", 7,))
		self.scritta2_5_2.grid(column=3,row=7,padx=10,pady=10)
		
		self.lunMin=tk.IntVar()
		self.lunMin.set(2)
		self.spinbox_row=ttk.Spinbox(self.config_label,from_=2,to=6,wrap=True,textvariable=self.lunMin,state='readonly')
		self.spinbox_row.grid(column=1,row=7,padx=10,pady=10)
		
		self.interfaccia_label = tk.LabelFrame(self.MainFrame,text="Interfaccia",font=("Courier", 10,"bold"),width=50)
		self.interfaccia_label.pack(pady=40)
		
		self.dsNavi=tk.Button(self.interfaccia_label,text="Dysplay navi",bg="Red")
		self.dsNavi.config(command=partial(self.changeColor,self.dsNavi))
		self.dsNavi.grid(row=0,column=0,padx=20,pady=10)
		
		self.dsPlayer=tk.Button(self.interfaccia_label,text="Dysplay player",bg="Red")
		self.dsPlayer.config(command=partial(self.changeColor,self.dsPlayer))
		self.dsPlayer.grid(row=0,column=1,padx=20,pady=10)
		
		self.dsTempo=tk.Button(self.interfaccia_label,text="Dysplay tempo",bg="Red")
		self.dsTempo.config(command=partial(self.changeColor,self.dsTempo))
		self.dsTempo.grid(row=0,column=2,padx=20,pady=10)





if __name__ == "__main__":
	window=Impostazioni()
	window.mainloop()
