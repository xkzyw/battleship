import tkinter as tk
from tkinter import ttk
import sys
sys.path.insert(0,"../src/objects")
from game import Game
class ConfigWindow (tk.Tk,Game):
    #Viene dopo PreGame
    def __init__(self):
        super().__init__()
        self.title("Battleship: configurazione iniziale")
        self.geometry("550x300")
        self.resizable(False,False)
        self.setStyles()
        self.createWidgets()
    #------------------------WIDGETS
    def setStyles(self):
        #per settare eventuali stili per i widgets
        pass
    def loadNext(self):
        # salva i dati e chiude la finestra
        #controllo sulla selezione del colore e l'inserimento del nome(Senon è stato scelto un colore e se non è stato inserito un nome, non posso chiudere la finestra)
        #prende nome del secondo giocatore e lo salva nella classe Game
        #......
        #dopo 
        self.destroy()
    def placeSingle(self):
        self.firstFrame=tk.Frame(self, border=0, relief="solid")
        self.firstFrame.pack()
        self.lbl_title=tk.Label(self.firstFrame, text="Singleplayer", font="Arial 20 bold")
        self.lbl_title.pack()
        self.secondFrame=tk.Frame(self, bg="gray", border=0,relief="solid",height=200,width=500)
        self.secondFrame.pack()
        opzioni=["green","yellow","red","blue"]
        #first player
        self.player1Frame=tk.Frame(self.secondFrame, border=1,relief="solid", height=200,width=250)
        self.player1Frame.grid(row=0,column=0)
        self.lbl_player1=tk.Label(self.player1Frame,text="Player 1", font="Arial 15")
        self.lbl_player1.pack()
        #entry con il nome del giocatore disabilitatà
        self.player1Name=tk.Entry(self.player1Frame, text=Game.player1.name, state='readonly', border=1, relief='solid', font="Courier 8")
        self.player1Name.pack(pady=30,padx=50)
        self.lbl_color=tk.Label(self.player1Frame, text="Colore:",font="Courier 15")
        self.lbl_color.pack(anchor='w',pady=10)
        #Conversione scelta colori -- combobox
        self.combo1=ttk.Combobox(self.player1Frame,values=opzioni)
        self.combo1.pack()
        self.thirdFrame=tk.Frame(self, height=200, width=500, bg='orange')
        self.thirdFrame.pack(pady=20)
        self.btn_continue=tk.Button(self.thirdFrame,text="Continua", font="Arial 12", height=2,width=30, bd=1,relief='raised', command=self.loadNext)
        self.btn_continue.pack(side='bottom')
    #--------------------------------------------------
    def placeMulti(self):
        self.firstFrame=tk.Frame(self, border=0, relief="solid")
        self.firstFrame.pack()
        self.lbl_title=tk.Label(self.firstFrame, text="Multiplayer", font="Arial 20 bold")
        self.lbl_title.pack()
        self.secondFrame=tk.Frame(self, bg="gray", border=0,relief="solid",height=200,width=500)
        self.secondFrame.pack()
        opzioni=["green","yellow","red","blue"]
        #first player
        self.player1Frame=tk.Frame(self.secondFrame, border=1,relief="solid", height=200,width=250)
        self.player1Frame.grid(row=0,column=0)
        self.lbl_player1=tk.Label(self.player1Frame,text="Player 1", font="Arial 15")
        self.lbl_player1.pack()
        #entry con il nome del giocatore disabilitatà
        self.player1Name=tk.Entry(self.player1Frame, text=Game.player1.name, state='readonly', border=1, relief='solid', font="Courier 8")
        self.player1Name.pack(pady=30,padx=50)
        self.lbl_color=tk.Label(self.player1Frame, text="Colore:",font="Courier 15")
        self.lbl_color.pack(anchor='w',pady=10)
        #Conversione scelta colori -- combobox
        self.combo1=ttk.Combobox(self.player1Frame,values=opzioni)
        self.combo1.pack()
        # self.btn_color1=tk.Button(self.player1Frame, bg="green", height=1,width=8)
        # self.btn_color2=tk.Button(self.player1Frame, bg="yellow",height=1, width=8)
        # self.btn_color3=tk.Button(self.player1Frame, bg="red", height=1, width=8)
        # self.btn_color4=tk.Button(self.player1Frame, bg="blue",height=1, width=8)
        # self.btn_color1.pack(side='left')
        # self.btn_color2.pack(side='left')
        # self.btn_color3.pack(side='left')
        # self.btn_color4.pack(side='left')
        #second player
        self.player2Frame=tk.Frame(self.secondFrame, border=1, relief='solid', height=200,width=250)
        self.player2Frame.grid(row=0,column=1)
        self.lbl_player2=tk.Label(self.player2Frame,text="Player 2", font="Arial 15")
        self.lbl_player2.pack()
        #entry con il nome del giocatore abilitatà
        self.player2Name=tk.Entry(self.player2Frame, state='normal', border=1, relief='solid', font="Courier 8")
        self.player2Name.pack(pady=30,padx=50)
        self.lbl_color2=tk.Label(self.player2Frame, text="Colore:",font="Courier 15")
        self.lbl_color2.pack(anchor='w',pady=10)
        self.combo2=ttk.Combobox(self.player2Frame,values=opzioni)
        self.combo2.pack()
        # self.btn_col1=tk.Button(self.player2Frame, bg="green", height=1,width=8)
        # self.btn_col2=tk.Button(self.player2Frame, bg="yellow",height=1, width=8)
        # self.btn_col3=tk.Button(self.player2Frame, bg="red", height=1, width=8)
        # self.btn_col4=tk.Button(self.player2Frame, bg="blue",height=1, width=8)
        # self.btn_col1.pack(side='left')
        # self.btn_col2.pack(side='left')
        # self.btn_col3.pack(side='left')
        # self.btn_col4.pack(side='left')
        #Ultimo frame
        self.thirdFrame=tk.Frame(self, height=200, width=500, bg='orange')
        self.thirdFrame.pack(pady=20)
        self.btn_continue=tk.Button(self.thirdFrame,text="Continua", font="Arial 12", height=2,width=30, bd=1,relief='raised', command=self.loadNext)
        self.btn_continue.pack(side='bottom')
        #piazzare bene i widgets
        # self.lbl_title.place(bordermode="outside", x=250, y=150)
    #-------------------------------------------
    def createWidgets(self):
        #controllo su istanza di game (modalità di gioco)
        #2 funzione che creano widget per la visualizzazione in base al risultato (1 per single player, uno per multiplayer)
        #......
        #non cancellare
        #self.placeSingle()
        self.placeMulti()
    #end function    
#end class
def main():
    i=ConfigWindow()
    i.mainloop()
#end function
main()