import tkinter as tk
from tkinter import messagebox
from tkinter.simpledialog import askstring
from src.objects.game import Game
class pre_game(tk.Tk):
	def __init__(self, game, nome_win="pre_game"):
		super().__init__()
		self.game=game
		self.title("Battaglia Navale")
		self.geometry("300x200")
		self.resizable(0, 0)
		self.CreateWidget()
	def CreateWidget(self):
		self.nome=tk.StringVar()
		self.MainFrame=tk.Frame(self)
		self.MainFrame.pack()
		self.TitleFrame=tk.Frame(self.MainFrame)
		self.TitleFrame.grid()
		self.titolo=tk.Label(self.TitleFrame,text="Battaglia Navale",font=("Courier", 20))
		self.titolo.grid(row=0,column=0)
		self.NameFrame=tk.Frame(self.MainFrame)
		self.NameFrame.grid()
		self.button_insert=tk.Button(self.NameFrame,text="inserisci il nome: ",command=self.insert_name,font=("Courier", 10))
		self.button_insert.grid(row=0,column=0)
		self.name_label_t=tk.Label(self.NameFrame,text="nome utente: ",font=("Courier", 10))
		self.name_label_t.grid(row=1,column=0)
		self.name_label=tk.Label(self.NameFrame,textvariable=self.nome,font=("Courier", 10))
		self.name_label.grid(row=2,column=0)
		self.ModeFrame=tk.Frame(self.MainFrame)
		self.ModeFrame.grid()
		self.mode_play=tk.StringVar(value="singleplayer")
		self.singleplayer=tk.Checkbutton(self.ModeFrame,text="singleplayer",variable=self.mode_play,onvalue="singleplayer",offvalue="multiplayer",font=("Courier", 10))
		self.singleplayer.grid(row=0,column=0)
		self.multiplayer=tk.Checkbutton(self.ModeFrame,text="multiplayer",variable=self.mode_play,onvalue="multiplayer",offvalue="singleplayer",font=("Courier", 10))
		self.multiplayer.grid(row=0,column=1)
		self.BFrame=tk.Frame(self.MainFrame)
		self.BFrame.grid()
		self.button_start=tk.Button(self.BFrame,text="inizia",command=self.start,state="disable",font=("Courier", 10))
		self.button_start.grid(row=0,column=0)

		self.button_remove=tk.Button(self.BFrame,text="rimuovi utente: ",command=self.remove_name,state="disable",font=("Courier", 10))
		self.button_remove.grid(row=1,column=0)
	def remove_name(self):
		print(self.nome.get())
		self.nome.set("")
		self.button_start["state"]="disable"
		self.button_remove["state"]="disable"

	def start(self):
		dati={
			"nome":self.nome.get(),
			"mode":self.mode_play.get()
		}
		print("ricorda i dati inseriti: ",dati)
		if dati["mode"] == "singleplayer":
			self.game.player1.type_in = "P"
			self.game.player1.name =  self.nome.get()
			self.game.player2.type_in = "PC"
			self.game.player2.name =  "PC"
			print("apri la finestra singleplayer")
		else:
			self.game.player1.type_in = "P"
			self.game.player1.name =  self.nome.get()
			self.game.player2.type_in = "P"
			print("apri la finestra multiplayer")

		self.game.start = True
	def insert_name(self):
		nome=askstring("nome", "inserisci nome: ")
		if nome == "" or nome == None:
			messagebox.showerror('Error', 'Non hai inserito il nome')
		else:
			messagebox.showinfo('Accettato', 'utente: {}'.format(nome))
			self.nome.set(str(nome))
			self.button_start["state"]="active"
			self.button_remove["state"]="active"

if __name__ == "__main__":
	w=pre_game()
	w.mainloop()
